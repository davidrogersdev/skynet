﻿SET IDENTITY_INSERT [dbo].[EvilMessages] ON
INSERT [dbo].[EvilMessages] ([Id], [MessageOfDoom]) VALUES (1, N'This is a message from the machines.')
INSERT [dbo].[EvilMessages] ([Id], [MessageOfDoom]) VALUES (2, N'You will all be destroyed.')
INSERT [dbo].[EvilMessages] ([Id], [MessageOfDoom]) VALUES (3, N'Prepare to yield to your fate.')
SET IDENTITY_INSERT [dbo].[EvilMessages] OFF

GO

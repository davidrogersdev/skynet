﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using Skynet.Ui.Controllers;
using Skynet.Ui.Models;
using Skynet.Ui.Services;

namespace Skynet.Ui.Tests
{
    [TestClass]
    public class HomeControllerTests
    {
        [TestMethod]
        public void ViewModel_Created_With_Persisted_Message()
        {
            // Arrange
            var m = Substitute.For<IEvilMessagesService>();
            m.GetAll().Returns(new List<string> {"Message 1", "Message 2"});

            var homeController = new HomeController(m);

            // Act
            homeController.Index();

            // Assert
            m.Received().GetAll();
        }
        
        [TestMethod]
        public void ViewModel_Created_With_Persisted_Messagej()
        {
            // Arrange
            var m = Substitute.For<IEvilMessagesService>();
            m.GetAll().Returns(new List<string> { "Message 1", "Message 2" });

            var homeController = new HomeController(m);

            // Act
            var result = homeController.Index();

            // Assert
            var messageViewViewModel = ((MessageViewViewModel)((ViewResult)result).ViewData.Model);

            CollectionAssert.AreEqual(messageViewViewModel.Messages.ToList(), m.GetAll().ToList());
        }
    }
}

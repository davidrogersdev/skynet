﻿using System.Linq;
using System.Web.Mvc;
using Skynet.Ui.Models;
using Skynet.Ui.Services;

namespace Skynet.Ui.Controllers
{
    public class HomeController : Controller
    {
        private readonly IEvilMessagesService _evilMessagesService;

        public HomeController(IEvilMessagesService evilMessagesService)
        {
            _evilMessagesService = evilMessagesService;
        }

        public ActionResult Index()
        {
            var messages = _evilMessagesService.GetAll();

            var messageViewViewModel = new MessageViewViewModel
            {
                Messages = messages.ToList()
            };

            return View(messageViewViewModel);
        }

    }
}
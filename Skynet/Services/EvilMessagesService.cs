﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;

namespace Skynet.Ui.Services
{
    public class EvilMessagesService : IEvilMessagesService
    {
        public IEnumerable<string> GetAll()
        {
            List<string> messagesList = new List<string>();

            using (var conn = new SqlConnection())
            {
                conn.ConnectionString = WebConfigurationManager.ConnectionStrings["SkynetConnection"].ConnectionString;
                conn.Open();

                using (var command = new SqlCommand())
                {
                    command.Connection = conn;
                    command.CommandText = "SELECT MessageOfDoom FROM EvilMessages;";
                    command.CommandType = CommandType.Text;


                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            messagesList.Add(reader.GetString(0));
                        }
                    }
                }
            }

            return messagesList;
        }
    }

    public interface IEvilMessagesService
    {
        IEnumerable<string> GetAll();
    }
}
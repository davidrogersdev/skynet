﻿using System.Collections.Generic;

namespace Skynet.Ui.Models
{
    public class MessageViewViewModel
    {
        public IEnumerable<string> Messages { get; set; }
    }
}
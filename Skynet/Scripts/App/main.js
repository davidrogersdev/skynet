﻿$(function() {

    var div = '<div class="initial-hide msg-panel"></div>';
    
    (function showMessagesOfDoom(jq, messagesList) {
        jq.append($(div).append(messagesList[0]).fadeIn("slow", function () {
            (messagesList = messagesList.slice(1)).length && showMessagesOfDoom(jq, messagesList);
        }));
    })($('div#mainContent'), messageData.messages);

});
